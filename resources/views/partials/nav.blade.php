<nav class="navbar navbar-default navbar-expand-lg navbar-light bg-light shadow-1" style="margin-top: 2vh;">
    <button style="border: none" class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-align-justify"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle blue-text" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Over the Counter
                </a>
                <div class="dropdown-menu mega-menu card-dropdown" aria-labelledby="navbarDropdown">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="orange-text" style="font-weight: bolder;">
                                Over the Counter
                            </h3>
                        </div>
                        <div class="col-md-3" style="border-left: 1px #ffc401 solid;">
                            <p class="grey-text"><u>Browse By Category</u></p>
                            <p><a href="{{url('/login')}}">Prescription</a></p>
                            <p><a href="{{url('/category/cough')}}">Cough, Flu, Allergy</a></p>
                            <p><a href="{{url('/category/pain-management')}}">Pain Management</a></p>
                            <p><a href="{{url('/category/wound-management')}}">Wound Management</a></p>
                            <p><a href="{{url('/category/skin-conditions')}}">Skin Conditions</a></p>
                            <p><a href="{{url('/category/oral-care')}}">Oral Care</a></p>
                            <p><a href="{{url('/category/medical-consumables')}}">Medical Consumable and Devices</a></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle blue-text" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Supplements
                </a>
                <div class="dropdown-menu mega-menu card-dropdown" aria-labelledby="navbarDropdown">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="orange-text" style="font-weight: bolder">
                                Supplements </h3>
                        </div>
                        <div class="col-md-3" style="border-left: 1px #ffc401 solid;">
                            <p class="grey-text"><u>Browse By Category</u></p>
                            <p><a href="{{url('/category/nutrional')}}">Nutritional</a></p>
                            <p><a href="{{url('/category/baby')}}">Baby</a></p>
                            <p><a href="{{url('/category/sports-nutrition')}}">Sports Nutrition</a></p>
                            <p><a href="{{url('/category/vitamins')}}">Vitamins</a></p>
                            <p><a href="{{url('/category/herbal')}}">Herbal</a></p>
                            <p><a href="{{url('/category/pregnancy')}}">Pregnancy</a></p>
                        </div>


                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle blue-text" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Toiletries and Personal Health Care
                </a>
                <div class="dropdown-menu mega-menu card-dropdown" aria-labelledby="navbarDropdown">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="orange-text" style="font-weight: bolder;;">
                                Toiletries and Personal Health Care
                            </h3>
                        </div>
                        <div class="col-md-4" style="border-left: 1px #ffc401 solid;">
                            <p class="grey-text"><u>Browse By Category</u></p>
                            <p><a href="{{url('/category/body-skin-care')}}">Body Skin Care</a></p>
                            <p><a href="{{url('/category/deodorant')}}">Deodorant</a></p>
                            <p><a href="{{url('/category/hair-care')}}">Hair Care</a></p>
                            <p><a href="{{url('/category/make-up')}}">Make Up</a></p>
                            <p><a href="{{url('/category/sun-protection')}}">Sun Protection</a></p>
                        </div>

                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle blue-text" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Baby Products
                </a>
                <div class="dropdown-menu mega-menu card-dropdown" aria-labelledby="navbarDropdown">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="orange-text" style="font-weight: bolder;">
                                Baby Products
                            </h3>
                        </div>
                        <div class="col-md-4" style="border-left: 1px #ffc401 solid;">
                            <p class="grey-text"><u>Browse By Category</u></p>
                            <p><a href="{{url('/category/nappies')}}">Nappies and Wipes</a></p>
                            <p><a href="{{url('/category/pregnancy')}}">Pregnancy and Planning for Baby</a></p>
                            <p><a href="{{url('/category/hair-care')}}">Hair Care</a></p>
                            <p><a href="{{url('/category/baby-feeding')}}">Baby Feeding</a></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="{{url('/my-cart')}}">My Cart
                ({{\Gloudemans\Shoppingcart\Facades\Cart::count()}}) <i
                        class="fa fa-shopping-cart"></i></a>
        </form>
    </div>
</nav>