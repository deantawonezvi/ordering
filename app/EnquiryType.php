<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class EnquiryType extends Model
{
    use Uuids;
}
