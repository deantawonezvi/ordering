<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    use Uuids;
}
