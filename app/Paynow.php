<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Paynow extends Model
{
    use Uuids;
    protected $guarded = [];
}
