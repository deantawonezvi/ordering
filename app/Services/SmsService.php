<?php


namespace App\Services;

use GuzzleHttp;
use Illuminate\Support\Facades\Log;


class SmsService
{

    public static function sendSms($mobile, $message)
    {
        $client = new GuzzleHttp\Client(['verify' => false]);
        $request_string = env('SMS_BASE_URL') . "&u=" . env('SMS_USERNAME') . "&h=" . env("SMS_TOKEN") . "&op=pv" . "&to=" . $mobile . "&msg=" . $message;

        try {


            $result = $client->get($request_string);
            $response = $result->getBody()->getContents();

            Log::info("=========================SMS===============================");
            Log::info("Request: ".$request_string);
            Log::info("Response: ".$response);
            Log::info("===========================================================");
        } catch (\Exception $e) {
            Log::info("=========================SMS ERROR===============================");
            Log::info("Request: ".$request_string);
            Log::alert("Request: ".$e);
            Log::info("===========================================================");
        }


    }

}