<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use Uuids;
}
