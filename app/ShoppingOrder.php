<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ShoppingOrder extends Model
{
    use Uuids;
    protected $guarded = [];

}
