<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class DeliveryNote extends Model
{
    use Uuids;
}
