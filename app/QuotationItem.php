<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class QuotationItem extends Model
{
    use Uuids;
}
