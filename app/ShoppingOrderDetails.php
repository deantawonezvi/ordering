<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

class ShoppingOrderDetails extends Model
{
    use Uuids;
    protected $guarded = [];

}
