<?php

use App\EnquiryType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnquiryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enquiry_types')->delete();

        EnquiryType::create(array(
                                 'name' => 'Prescription', 'description' => 'project-1'

        ));

        EnquiryType::create(array(
                                 'name' => 'OTC', 'description' => 'project-2'

        ));

        EnquiryType::create(array(
            'name' => 'Consultation Request', 'description' => 'project-2'

        ));
        EnquiryType::create(array(
            'name' => 'Lab Request', 'description' => 'project-2'

        ));

        EnquiryType::create(array(
            'name' => 'Radiology Request ', 'description' => 'project-2'

        ));


        EnquiryType::create(array(
                                 'name' => 'Other Products',
                                  'description' => 'project-3'

        ));
    }
}
